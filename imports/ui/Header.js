import React from 'react';
import { Meteor } from 'meteor/meteor';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Box, Button, Menu, MenuItem } from '@material-ui/core';
import { useTracker } from 'meteor/react-meteor-data';

const useStyles = makeStyles({
  toolbar: {
    textAlign: 'center',
    height: 80,
  },
  heading: {
    margin: 'auto',
  },
  avatar: {
    color: 'white',
    position: 'absolute',
    right: '20px',
  },

  backgroundWhite: {
    color: '#000000',
    backgroundColor: '#ffffff',
  },

  white: {
    color: 'white',
  },
});

const Header = ({ incompleteCount }) => {
  const user = useTracker(() => Meteor.user());
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const logout = () => {
    setAnchorEl(null);
    Meteor.logout();
  };

  return (
    <AppBar position="static">
      <Toolbar className={classes.toolbar}>
        <Typography className={classes.heading} variant="h5" align="center">
          Todo App {incompleteCount ? `(${incompleteCount} Incomplete)` : ''}
        </Typography>
        <div className={classes.avatar}>
          <Button
            aria-controls="simple-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            <Avatar className={classes.backgroundWhite}>
              {user && user.username.charAt(0)}
            </Avatar>
            <Box className={classes.white} ml={2}>
              {user.username}
            </Box>
          </Button>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}
          >
            <MenuItem onClick={handleClose}>Profile</MenuItem>
            <MenuItem onClick={handleClose}>My account</MenuItem>
            <MenuItem onClick={logout}>Logout</MenuItem>
          </Menu>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
