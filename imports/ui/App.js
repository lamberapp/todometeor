import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Meteor } from 'meteor/meteor';
import { useTracker } from 'meteor/react-meteor-data';
import { withTracker } from 'meteor/react-meteor-data';
import { Tasks } from '../api/tasks';
import Header from './Header';
import Form from './Form';
import TodoList from './Todolist';
import { Button } from '@material-ui/core';
import SignIn from './SignIn';

const App = ({ tasks, incompleteCount }) => {
  const [hideCompleted, setHideCompleted] = useState(true);
  const [editItem, setEditItem] = useState();
  const user = useTracker(() => Meteor.user());

  const useStyles = makeStyles({
    root: {
      textAlign: 'center',
      height: '100%',
    },
  });
  const classes = useStyles();

  return user ? (
    <div className="app">
      <div className={classes.root}>
        <Header incompleteCount={incompleteCount} />
        <Form editItem={editItem} setEditItem={setEditItem} />
        <TodoList
          tasks={tasks}
          hideCompleted={hideCompleted}
          setEditItem={setEditItem}
          editItem={editItem}
        />
        <Button
          onClick={() => setHideCompleted(!hideCompleted)}
          variant="outlined"
          color="primary"
        >
          {hideCompleted ? 'Show All' : 'Hide Completed'}
        </Button>
      </div>
    </div>
  ) : user !== undefined ? (
    <SignIn />
  ) : null;
};

export default withTracker(() => {
  Meteor.subscribe('tasks');

  return {
    tasks: Tasks.find({}, { sort: { checked: 1, createdAt: -1 } }).fetch(),
    incompleteCount: Tasks.find({ checked: { $ne: true } }).count(),
  };
})(App);
