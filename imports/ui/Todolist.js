import React from 'react';
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { Chip, Button, Box } from '@material-ui/core';

function TodoList({ tasks = [], hideCompleted, setEditItem, editItem = {} }) {
  const userId = Meteor.user() ? Meteor.user()['_id'] : null;

  const togglePrivate = (taskId, setTaskPrivate) => {
    Meteor.call('tasks.setPrivate', taskId, setTaskPrivate);
  };

  const toggleChecked = (taskId, setTaskChecked) => {
    Meteor.call('tasks.setChecked', taskId, setTaskChecked);
  };

  const filteredTasks = hideCompleted
    ? tasks.filter((task) => !task.checked)
    : tasks;

  const useStyles = makeStyles({
    container: {
      padding: 16,
    },
    taskComplete: {
      textDecoration: 'line-through',
    },
  });

  const classes = useStyles();

  const handleEdit = (item) => {
    setEditItem(item);
  };

  const handleDelete = (taskId) => {
    Meteor.call('tasks.delete', taskId);
  };

  return (
    <Container className={classes.container} maxWidth="md">
      {!filteredTasks.length ? (
        <Typography variant="h6">
          0 incomplete tasks
        </Typography>
      ) : (
        <List>
          {filteredTasks.map((task) => {
            return (
              <div className={task.checked ? classes.taskComplete : null}>
                <ListItem
                  selected={editItem && editItem._id === task._id}
                  key={task._id}
                  button
                >
                  <ListItemIcon
                    onClick={() => toggleChecked(task._id, !task.checked)}
                  >
                    {task.checked ? (
                      <CheckCircleIcon color="grey" />
                    ) : (
                      <RadioButtonUncheckedIcon color="primary" />
                    )}
                  </ListItemIcon>
                  <Box mr={5}>
                    <Chip
                      variant="outlined"
                      color={
                        task.checked
                          ? 'grey'
                          : task.private
                          ? 'primary'
                          : 'secondary'
                      }
                      label={task.private ? 'Private' : 'Public'}
                    />
                  </Box>

                  <ListItemText primary={task.text} />
                  <ListItemSecondaryAction>
                    {task.owner === userId ? (
                      <Button
                        onClick={() => togglePrivate(task._id, !task.private)}
                      >
                        Make {!task.private ? 'Private' : 'Public'}
                      </Button>
                    ) : (
                      <Button disabled>Owner: {task.username}</Button>
                    )}
                    <IconButton
                      edge="end"
                      aria-label="edit"
                      onClick={() => handleEdit(task)}
                      color={task.checked ? 'grey' : 'primary'}
                    >
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      edge="end"
                      aria-label="delete"
                      onClick={() => handleDelete(task._id)}
                      color={task.checked ? 'grey' : 'primary'}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              </div>
            );
          })}
        </List>
      )}
    </Container>
  );
}

export default TodoList;
