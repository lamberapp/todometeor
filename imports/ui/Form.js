import React, { useRef, useState, useEffect } from 'react';
import { Meteor } from 'meteor/meteor';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Container from '@material-ui/core/Container';
import Button from '@material-ui/core/Button';
import { Box } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    marginTop: 16,
    marginBottom: 16,
    padding: 16,
    boxShadow:
      '0px 3px 5px -1px rgba(0,0,0,0.2), 0px 6px 10px 0px rgba(0,0,0,0.14), 0px 1px 18px 0px rgba(0,0,0,0.12)',
  },
  button: {
    marginTop: 16,
  },
});

const Form = ({ editItem, setEditItem, error }) => {
  const textInput = useRef();
  const classes = useStyles();
  const [taskText, setTaskText] = useState('');

  useEffect(() => {
    if (editItem) {
      setTaskText(editItem.text);
    }
  }, [editItem]);

  const handleChange = (event) => {
    const taskText = event.target.value;
    setTaskText(taskText);
  };

  const resetForm = () => {
    setEditItem(null);
    setTaskText('');
  };

  const handleSubmit = () => {
    if (editItem) {
      Meteor.call('tasks.edit', editItem._id, taskText);
    } else {
      Meteor.call('tasks.insert', taskText);
    }

    resetForm();
  };

  return (
    <Container maxWidth="sm" className={classes.root}>
      <Grid container alignItems="center">
        <Grid item md={12}>
          <TextField
            ref={textInput}
            value={taskText}
            onChange={handleChange}
            error={!!error}
            helperText={error}
            id="outlined-basic"
            fullWidth
            label="Enter Task"
            multiline
            variant="outlined"
          />
        </Grid>
        <Grid container direction="row" justify="center" alignItems="center">
          {editItem ? (
            <Box mr={2}>
              <Button
                className={classes.button}
                variant="contained"
                color="primary"
                onClick={resetForm}
              >
                Cancel
              </Button>
            </Box>
          ) : null}
          <Box>
            <Button
              className={classes.button}
              variant="contained"
              color="primary"
              onClick={handleSubmit}
            >
              {editItem ? 'Edit' : 'Add'}
            </Button>
          </Box>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Form;
